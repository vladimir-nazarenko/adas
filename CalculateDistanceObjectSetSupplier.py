import ObjectSetSupplier

import numpy as np


class CalculateDistanceObjectSetSupplier(ObjectSetSupplier.ObjectSetSupplier):
    def __init__(self, distance_map_loader, supplier):
        super().__init__(supplier)
        self._dm_loader = distance_map_loader

    def _process(self, object_set):
        for obj in object_set:
            obj.distance = self._estimate_distance(obj)
        return object_set

    def _estimate_distance(self, obj):
        distances = self._dm_loader.get()
        obj_distances = distances[obj.u0:obj.u0 + obj.height, obj.v0:obj.v0 + obj.width]
        if obj_distances.size > 0:
            distance = np.percentile(obj_distances, 5)
        else:
            distance = np.NaN
        return distance
