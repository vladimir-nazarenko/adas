import numpy as np


class Calibration:
    def __init__(self, path="000000.txt"):
        p = []
        with open(path) as calib:
            p.append(calib.readline())
            p.append(calib.readline())
        p = [x.split()[1:] for x in p]
        p = [[float(y) for y in x] for x in p]
        p = [np.reshape(x, (3, 4)) for x in p]
        focal = p[0][0, 0]
        baseline = p[1][0, 3] / focal  # in meters
        c_l = p[0][0:2, 2]  # in pix
        c_r = p[1][0:2, 2]  # in pix

        Q = np.identity(4, dtype=np.float64)
        Q[0:2, 3] = -c_l
        Q[2, 2] = 0
        Q[2, 3] = focal
        Q[3, 2] = 1 / (-baseline)
        Q[3, 3] = (c_l[0] - c_r[0]) / baseline
        self.Q = Q
