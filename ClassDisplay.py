import Display
import cv2


class ClassDisplay(Display.Display):
    def __init__(self, next_display):
        super().__init__(next_display)

    def _draw(self):
        for obj in self._objects:
            u1 = obj.u0 + obj.height
            cv2.putText(self._canvas, obj.class_name, (obj.v0, u1), cv2.FONT_HERSHEY_PLAIN,
                        1,
                        (1, 1, 1), thickness=2)
            cv2.putText(self._canvas, obj.class_name, (obj.v0, u1), cv2.FONT_HERSHEY_PLAIN,
                        1,
                        (255, 255, 255), thickness=1)
