import ObjectSetSupplier
import resolver
import numpy as np
import cv2


class CollisionEstimatorObjectSetSupplier(ObjectSetSupplier.ObjectSetSupplier):
    def __init__(self, supplier):
        super().__init__(supplier)
        self._roi = None

    def _process(self, object_set):
        self._get_roi()
        for obj in object_set:
            if np.sum(self._roi[obj.u0:obj.u0+obj.height, obj.v0:obj.v0+obj.width]) > 0:
                obj.ttc = max((obj.distance - 2.5) / resolver.speed_loader.get(), 0)
                # print(obj.ind, obj.distance, resolver.speed_loader.get())
                # print(obj.ind, obj.distance, resolver.speed_loader.get())
        return object_set

    def _get_roi(self):
        resolver.horizon_estimator.estimate_horizon()
        horizon = resolver.horizon_estimator.horizon
        im_height = resolver.image_loader.get_left().shape[0] - 1
        im_width = resolver.image_loader.get_left().shape[1]
        pts = np.array([[im_height, im_width // 2 - 200], [horizon, im_width // 2], [im_height, im_width // 2 + 200]])
        pts[:, [0, 1]] = pts[:, [1, 0]]
        roi = np.zeros(shape=(im_height, im_width), dtype=np.uint8)
        roi = cv2.fillConvexPoly(roi, pts, 255)
        # cv2.imshow("roi", roi)
        self._roi = roi

    def _estimate_distance(self, obj):
        distances = self._dm_loader.get()
        obj_distances = distances[obj.u0:obj.u0 + obj.height, obj.v0:obj.v0 + obj.width]
        if obj_distances.size > 0:
            distance = np.percentile(obj_distances, 5)
        else:
            distance = np.NaN
        return distance
