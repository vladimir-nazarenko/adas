import Display
from Provider import Provider

import cv2


class DefaultDisplay(Display.Display):
    def __init__(self, next_display):
        super().__init__(next_display)

    def _draw(self):
        for obj in self._objects:
            if obj.provider == Provider.YOLO:
                color = (255, 0, 0)
            elif obj.provider == Provider.KLT:
                color = (0, 255, 0)
            else:
                color = (0, 0, 255)
            u1 = obj.u0 + obj.height
            v1 = obj.v0 + obj.width
            cv2.rectangle(self._canvas, (obj.v0, obj.u0), (v1, u1), color, 1)
            # cv2.putText(self._canvas, obj.class_name, (obj.v0, u1), cv2.FONT_HERSHEY_PLAIN,
            #             1,
            #             (1, 1, 1), thickness=2)
            # cv2.putText(self._canvas, obj.class_name, (obj.v0, u1), cv2.FONT_HERSHEY_PLAIN,
            #             1,
            #             (255, 255, 255), thickness=1)
