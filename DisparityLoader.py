import disparitySGBM
import resolver
import logging
import time
import SpeedLoader


class DisparityLoader:
    logger = logging.getLogger(__name__)

    def __init__(self, image_loader):
        self._image_loader = image_loader
        self._loader_state = None
        self._disparity = None
        self._config = resolver.config

    def _compute_and_set_disparity(self):
        left = self._image_loader.get_left()
        right = self._image_loader.get_right()
        start = time.time()
        self._disparity = disparitySGBM.get_disparity_sgbm(left, right, self._config)
        self.logger.info("Recalculated disparity in {0:.0f} ms".format(1000 * (time.time() - start)))

    def get(self):
        # if needed, reset disparity
        if self._loader_state is None or not self._image_loader.is_in_state(self._loader_state):
            self._loader_state = self._image_loader.get_state()
            self._compute_and_set_disparity()
        return self._disparity

    def get_state(self):
        return self._loader_state

    def is_in_state(self, state):
        return self._image_loader.is_in_state(state)


