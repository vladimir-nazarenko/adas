import cv2


class Display:
    def __init__(self, next_display):
        self._next_display = next_display
        self._canvas = None

    def display(self, objects, canvas):
        self._canvas = canvas
        self._objects = objects
        self._draw()
        self._show_telemetry()
        if self._next_display is not None:
            self._next_display.display(objects, canvas)
        else:
            self._show()

    def _draw(self):
        raise NotImplementedError

    def _show(self):
        assert self._canvas is not None, "Nothing to display"
        cv2.imshow("Canvas(Default)", self._canvas)

    def _show_telemetry(self):
        pass
