import resolver
import cv2
import numpy as np
import time
import logging


class DistanceMapLoader:
    logger = logging.getLogger(__name__)

    def __init__(self, disparity_loader):
        self._disparity_loader = disparity_loader
        self._loader_state = None
        self._distance_map = None

    def _compute_and_set_distance_map(self):
        calib = resolver.calibration
        reprojected = cv2.reprojectImageTo3D(self._disparity_loader.get(), calib.Q)
        distance = np.sqrt(reprojected[..., 2] ** 2 + reprojected[..., 1] ** 2)
        self._distance_map = distance

    def get(self):
        # if needed, reset
        if self._loader_state is None or not self._disparity_loader.is_in_state(self._loader_state):
            self._loader_state = self._disparity_loader.get_state()
            # just to be honest with time measurement
            self._disparity_loader.get()
            start = time.time()
            self._compute_and_set_distance_map()
            self.logger.info("Recalculated distance map in {0:.0f} ms".format(1000 * (time.time() - start)))
        return self._distance_map

    def get_state(self):
        return self._loader_state

    def is_in_state(self, state):
        return self._disparity_loader.is_in_state(state)