import ObjectSetSupplier
from KLTObjectSetSupplier import KLTObjectSetSupplier
import resolver

from Provider import Provider
import logging


class FilterKLTAtSidesObjectSetSupplier(ObjectSetSupplier.ObjectSetSupplier):
    logger = logging.getLogger(__name__)

    def _process(self, object_set):
        return self._filter_object_at_sides(object_set)

    def _filter_object_at_sides(self, object_set):
        bad_index = set()
        for i, obj in enumerate(object_set):
            if obj.provider == Provider.KLT and self._obj_is_at_side(obj):
                bad_index.add(i)
                KLTObjectSetSupplier.invalidate(obj)
                self.logger.debug("Removed KLT object with id {0} and class name {1} because it's at the side of image".
                                  format(obj.ind, obj.class_name))
        return [x for i, x in enumerate(object_set) if i not in bad_index]

    @staticmethod
    def _obj_is_at_side(obj):
        im_shape = resolver.image_loader.get_left().shape
        thresh1 = 5
        thresh2 = 0.8
        obj_is_at_side = obj.v0 < thresh1 or im_shape[1] - (obj.v0 + obj.width) < thresh1
        # obj_diminished = (obj.height / obj.initial_shape[0]) * (obj.width / obj.initial_shape[1]) < thresh2
        return obj_is_at_side
