import Display
import cv2
import resolver


class FrameNumberDisplay(Display.Display):
    def __init__(self, next_display):
        super().__init__(next_display)

    def _draw(self):
        label = str(resolver.image_loader.get_current_frame_number())
        cv2.putText(self._canvas, label, (10, 25), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                    1,
                    (1, 1, 1), thickness=5)
        cv2.putText(self._canvas, label, (10, 25), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                    1,
                    (255, 255, 255), thickness=3)
