import Display
import cv2
import numpy as np
import resolver


class HorizonDebugDisplay(Display.Display):
    def __init__(self, horizon_estimator, next_display):
        super().__init__(next_display)
        self._horizon_estimator = horizon_estimator

    def _draw(self):
        pass

    def _show_telemetry(self):
        img = np.dstack([self._horizon_estimator.vdisparity,
                         self._horizon_estimator.lines_image,
                         np.zeros_like(self._horizon_estimator.vdisparity)])
        img = np.hstack([img, cv2.cvtColor(self._horizon_estimator.morph_image, cv2.COLOR_GRAY2BGR),
                         cv2.cvtColor(self._horizon_estimator.edges_image, cv2.COLOR_GRAY2BGR)])
        cv2.imshow("Horizon_debug", img)
        # cv2.imshow("Stixel roi", self._stixel_estimator.stixel_roi)
        # left = resolver.image_loader.get_left()
        # cv2.imshow("Stixels", cv2.addWeighted(
        #     cv2.cvtColor(self._stixel_estimator.stixels, cv2.COLOR_GRAY2BGR), 1/3, left, 2/3, 0))

