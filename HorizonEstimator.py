import cv2
import resolver
import numpy as np
import time
import logging


class HorizonEstimator:
    logger = logging.getLogger(__name__)

    _tan_history = []
    _horizon_history = []
    _lines = None
    _bottom_line = None
    disparity = None
    vdisparity = None
    edges_image = None
    lines_image = None
    morph_image = None
    horizon = None
    tan = None

    def __init__(self, disparity_loader):
        config = resolver.config
        self._disparity_loader = disparity_loader
        self.memory = int(config["memory"])
        self.vdisparity_threshold = int(config["vdisparity_threshold"])
        self.vdisparity_kernel_size = int(config["vdisparity_kernel_size"])
        self.canny_a = int(config["canny_a"])
        self.canny_b = int(config["canny_b"])
        self.horizon_shift = int(config["horizon_shift"])
        self._loader_state = None

    # Median filter for horizon predictions. Aims to avoid false horizon estimations.
    def _filter(self, horizon=None, tan=None):
        if horizon is not None and tan is not None:
            self._tan_history.append(tan)
            self._horizon_history.append(horizon)
            # remove outdated measurements
            if len(self._tan_history) > self.memory:
                self._tan_history.pop()
            if len(self._horizon_history) > self.memory:
                self._horizon_history.pop()
        # retrieve median
        sorted_measurements = list([x for x in sorted(zip(self._horizon_history, self._tan_history))])
        median_horizon, median_tan = sorted_measurements[len(sorted_measurements) // 2]
        return median_horizon, median_tan

    def _compute_and_set_vdisparity(self):
        self.vdisparity = np.vstack(
            np.histogram(r, bins=np.arange(64))[0] for r in self.disparity).astype(np.uint8)

    # Performs line detection on vdisparity image. One of these lines describes the horizon.
    def _compute_and_set_lines(self):
        _, vdisparity = cv2.threshold(self.vdisparity, self.vdisparity_threshold, 255, cv2.THRESH_BINARY)
        ksize = self.vdisparity_kernel_size
        # vdisparity[0:vdisparity.shape[0] // 2] = 0
        # vdisparity[:, vdisparity.shape[1] // 2:] = 0
        vdisparity = cv2.morphologyEx(vdisparity, cv2.MORPH_CLOSE,
                                      cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (ksize, ksize)))
        self.morph_image = vdisparity

        # edges = cv2.Canny(vdisparity, self.canny_a, self.canny_b)

        edges = vdisparity
        self.edges_image = edges
        self._lines = cv2.HoughLinesP(edges, 15, np.pi / 35, 100, minLineLength=50, maxLineGap=95)

    # for now it just calculates the longest line. More sophisticated way may be applied
    @staticmethod
    def compute_area_under_line(v0, u0, v1, u1, im_height):
        max_area = 1000000
        dv = v0 - v1
        du = u0 - u1
        # threshold vertical lines
        if abs(dv) < 5:  # or abs(du) < 5:
            return max_area
        if dv * du < 0:
            return max_area
        # if du / dv < 0:
        #     return max_area
        if u1 < u0:
            u0, u1 = u1, u0
            v0, v1 = v1, v0
        sin = float(np.abs(du) / (np.sqrt(du ** 2 + dv ** 2)))
        cos = float(np.sqrt(1 - sin ** 2))
        a = u1 - v1 * sin
        b = v1 + (im_height - u0) * cos
        area = 1 / (dv * du)
        # area = (im_height - a) * b
        # area = dv * du
        return area

    # Choose the line for horizon on the image
    def _estimate_and_set_bottom_line(self):
        im_height = len(self.vdisparity)
        areas = map(lambda l: self.compute_area_under_line(*tuple(l[0]), im_height), self._lines)
        bottom_line = min(zip(areas, self._lines), key=lambda x: x[0])[1].reshape((-1))

        if bottom_line[1] > bottom_line[3]:
            bottom_line[1] -= self.horizon_shift
        else:
            bottom_line[3] -= self.horizon_shift
        self._bottom_line = bottom_line

    def _compute_and_set_horizon_tan(self):
        im_height = len(self.vdisparity)
        # horizon is the point where line on vdisparity hits the left side of vdisparity image
        v0, u0, v1, u1 = tuple(self._bottom_line)
        tan = (u1 - u0) / (v1 - v0)
        if np.isinf(tan):
            horizon = im_height // 2
        else:
            horizon = im_height - int(max(v0, v1) * tan + (im_height - max(u0, u1)))

        self.horizon, self.tan = self._filter(horizon, tan)
        # self.horizon, self.tan = horizon, tan

    def _draw_lines(self):
        lines_img = np.zeros_like(self.vdisparity)
        for l in self._lines:
            v0, u0, v1, u1 = l[0]
            cv2.line(lines_img, (v0, u0), (v1, u1), 100, 1)
        v0, u0, v1, u1 = self._bottom_line
        cv2.line(lines_img, (v0, u0), (v1, u1), 255, 1)
        cv2.line(self.vdisparity, (v0, u0), (v1, u1), 0, 2)

        self.lines_image = lines_img

    def _compute_and_set_horizon(self):
        self._compute_and_set_vdisparity()
        self._compute_and_set_lines()
        if self._lines is not None:
            self._estimate_and_set_bottom_line()
            self._draw_lines()
            self._compute_and_set_horizon_tan()
        else:
            self.horizon, self.tan = self._filter()
            self.logger.warn("Couldn't calculate horizon, setting from previous estimations")

    def estimate_horizon(self):
        if self._loader_state is None or not self._disparity_loader.is_in_state(self._loader_state):
            self._loader_state = self._disparity_loader.get_state()
            self.disparity = self._disparity_loader.get()
            start = time.time()
            self._compute_and_set_horizon()
            self.logger.info("Recalculated horizon in {0:.0f} ms".format(1000 * (time.time() - start)))

    def get_state(self):
        return self._loader_state

    def is_in_state(self, state):
        return self._disparity_loader.is_in_state(state)
