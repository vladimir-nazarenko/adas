import cv2
import glob
import resolver
import logging


# If you'd want a singleton
# class ImageLoader:
#     instance = None
#
#     def __init__(self):
#         if not ImageLoader.instance:
#             ImageLoader.instance = ImageLoader.__ImageLoader()
#
#     def __getattr__(self, name):
#         return getattr(self.instance, name)

class KITTIRawImageLoader:
    logger = logging.getLogger(__name__)

    class State:
        def __init__(self, frame):
            self._frame = frame

    def __init__(self):
        config = resolver.config
        kitti_path = config["kitti"]
        self._left_images_path = kitti_path + config["left"]
        self._right_images_path = kitti_path + config["right"]
        self._start_frame = int(config["start_frame"])
        if "end_frame" in config:
            self._end_frame = int(config["end_frame"])
        else:
            self._end_frame = len(glob.glob1(self._left_images_path, "*.png"))
        self._current_frame = self._start_frame - 1
        self._left = None
        self._right = None

    @staticmethod
    def not_initialized():
        raise RuntimeError("ImageLoader was not initialized. Call ImageLoader#next")

    def get_left(self):
        return self._left if self._left is not None else self.not_initialized()

    def get_right(self):
        return self._right if self._right is not None else self.not_initialized()

    def next(self):
        self._current_frame += 1
        self._left = cv2.imread(self._left_images_path + "/{0:010d}.png".format(self._current_frame))
        self._right = cv2.imread(self._right_images_path + "/{0:010d}.png".format(self._current_frame))
        self.logger.info("Loaded frame {0}".format(self._current_frame))

    def has_next(self):
        return self._current_frame < self._end_frame

    def get_current_frame_number(self):
        return self._current_frame

    def get_state(self):
        return self.State(self._current_frame)

    def is_in_state(self, state):
        return state._frame == self._current_frame
