import ObjectSetSupplier
from Provider import Provider
import resolver

import cv2
import numpy as np
import logging


# selects yolo objects from the input objects set and tracks them from frame to frame,
# spawning tracked objects on each object set request
class KLTObjectSetSupplier(ObjectSetSupplier.ObjectSetSupplier):
    logger = logging.getLogger(__name__)
    _TRACK_TTL = 0

    def __init__(self, track_frames_count, supplier):
        super().__init__(supplier)
        KLTObjectSetSupplier._TRACK_TTL = track_frames_count
        self._tracked_objects = []
        self._freshmen = []
        self._current_image = None

    def _process(self, object_set):
        self._current_image = resolver.image_loader.get_left()
        self._track(object_set)
        return self._concat(self._tracked_objects, object_set)

    def _track(self, object_set):
        self._flush_previous_freshmen()
        self._purge_outdated_objects()
        self._set_freshmen(object_set)
        self._update_tracked()

    # checks that a) object is still on the scene and not occluded and b) object was not purged by someone else
    # in case a or b are false, purges the object from the tracked set
    def _purge_outdated_objects(self):
        bad_index = set()
        for i, obj in enumerate(self._tracked_objects):
            if obj.ttl <= 0 or self._check_if_object_is_gone(obj):
                bad_index.add(i)
                self.logger.debug("Purged outdated object with id {0} and class name {1} due to {2}".format(
                    obj.ind, obj.class_name, "ttl" if obj.ttl <= 0 else "disappearing"))
        self._tracked_objects = [x for i, x in enumerate(self._tracked_objects) if i not in bad_index]

    # computes the optical flow between the initial image of the object and the current one.
    # if fraction of matched points is too small, claims object as disappeared
    def _check_if_object_is_gone(self, obj):
        prev_img = cv2.cvtColor(obj.initial_full_img, cv2.COLOR_BGR2GRAY)
        corners = obj.translated_corners(obj.initial_corners)
        # for c in corners:
        #     cv2.drawMarker(resolver.image_loader.get_left(), tuple(c[0]), (255, 0, 0))
        if corners is None or corners.size == 0:
            return True
        cur_img = cv2.cvtColor(self._current_image, cv2.COLOR_BGR2GRAY)
        new_corners, status, error = cv2.calcOpticalFlowPyrLK(prev_img, cur_img, corners, None)
        status = status.reshape((-1)) > 0
        # TOO: REMOVE DEBUG PRINT
        # for p1, p2, s, e in zip(corners, new_corners, status, error):
        #     if s and e < 30:
        #         # print(e)
        #         cv2.arrowedLine(self._current_image, tuple(p1[0]), tuple(p2[0]), (255, 0, 0), tipLength=0.2)
        # print(np.sum(status) / len(status) < 0.5)
        if np.sum(error) / len(error) > 140:
            self.logger.debug("Object with id {0} and class name {1} gone with mean corner error {2}".format(
                obj.ind, obj.class_name, np.sum(error) / len(error)))
        return np.sum(error) / len(error) > 50

    # flushes objects caught on previous frame into the tracked object pool
    def _flush_previous_freshmen(self):
        for obj in self._freshmen:
            if obj.ttl > 0:
                obj.provider = Provider.KLT
                obj.full_img = obj.initial_full_img
                obj.corners = obj.initial_corners
                self.logger.debug("Flushed freshman with id {0} and class name {1}".format(obj.ind, obj.class_name))
        self._tracked_objects.extend(self._freshmen)
        self._freshmen = []

    # adds and sets required fields
    # adds objects into tracked
    def _set_freshmen(self, object_set):
        img = resolver.image_loader.get_left()
        for obj in object_set:
            obj.initial_full_img = img
            # obj.initial_disparity_piece = resolver.disparity_loader.get()[obj.u0:obj.u0+obj.height, obj.v0:obj.v0+obj.width]
            obj.ttl = KLTObjectSetSupplier._TRACK_TTL
            obj.initial_shape = (obj.height, obj.width)
            self._set_pixels(obj, img)
            self._set_initial_corners(obj)
            self._freshmen.append(obj)
            self.logger.debug("Added freshman with id {0} and class name {1}".format(obj.ind, obj.class_name))

    # sets pixel field to the rectangular patch
    # of the image, corresponding to the object
    def _set_pixels(self, obj, img):
        v0, u0 = obj.v0, obj.u0
        v1, u1 = obj.v0 + obj.width, obj.u0 + obj.height
        obj.pixels = img[u0:u1, v0:v1]
        if obj.pixels is None or obj.pixels.size == 0:
            self.logger.critical("couldn't set pixels for object with label {0} and id {1}"
                                 .format(obj.class_name, obj.ind))

    # detects corners using Shi-Tomasi detector
    @staticmethod
    def _get_corners(obj, max_corners=300, thresh=0.001, distance=5):
        corners = cv2.goodFeaturesToTrack(
            cv2.cvtColor(obj.pixels, cv2.COLOR_BGR2GRAY), max_corners, thresh, distance)
        if corners is None or corners.size == 0:
            KLTObjectSetSupplier.logger.critical("couldn't set corners for object with label {0} and id {1}"
                                                 .format(obj.class_name, obj.ind))
            return np.array([])
        else:
            return corners.reshape((-1, 2))

    # sets initial corners using Shi-Tomasi detector
    @staticmethod
    def _set_initial_corners(obj):
        obj.initial_corners = KLTObjectSetSupplier._get_corners(obj, thresh=0.1)

    # shifts each of the tracked objects and updates fields
    def _update_tracked(self):
        for obj in self._tracked_objects:
            self._shift(obj)
            obj.full_img = self._current_image
            self._set_pixels(obj, self._current_image)
            # TODO: enable
            # obj.corners = self._get_corners(obj)
            obj.ttl -= 1
            # if obj.ttl == 1:
            #     obj.ttl = self._TRACK_TTL
            #     self._set_initial_corners(obj)

    # moves object's bounding box
    def _shift(self, obj):
        if obj.corners is None or obj.corners.size == 0:
            obj.ttl = 0
            self.logger.warn("Problem with corners on object {0} with label {1}".format(obj.ind, obj.class_name))
        else:
            corners, new_corners = self._get_corners_correspondence(obj)
            if corners.size > 0:
                self._move_object(obj, corners, new_corners)
            else:
                obj.ttl = 0

    # calculates L-K point correspondence
    def _get_corners_correspondence(self, obj):
        prev_img = cv2.cvtColor(obj.full_img, cv2.COLOR_BGR2GRAY)
        corners = obj.translated_corners(obj.corners)
        cur_img = cv2.cvtColor(self._current_image, cv2.COLOR_BGR2GRAY)
        new_corners, status, error = cv2.calcOpticalFlowPyrLK(prev_img, cur_img, corners, None)

        status = status.reshape((-1)) > 0
        corners = corners[status]
        new_corners = new_corners[status]
        return corners, new_corners

    # calculates median move and moves borders
    def _move_object(self, obj, corners, new_corners):
        median_shift = np.median((new_corners - corners).reshape((-1, 2)), axis=0)
        # obj.trajectory.append((obj.v0 + obj.width // 2, obj.u0 + obj.height // 2))
        if not obj.move(median_shift[0], median_shift[1], *self._current_image.shape[:2]):
            self.logger.warn("Object leaved the image, but is still tracked")

    # removes object from the tracked set
    @staticmethod
    def invalidate(obj):
        obj.ttl = 0

    @staticmethod
    def update_tracked_to_be_as_detected(tracked, detected):
        tracked.u0 = detected.u0
        tracked.v0 = detected.v0
        tracked.width = detected.width
        tracked.height = detected.height
        tracked.remove_last_point()
        tracked.add_to_trajectory((tracked.v0, tracked.u0))
        tracked.ttl = KLTObjectSetSupplier._TRACK_TTL


