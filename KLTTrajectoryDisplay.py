import Display
from Provider import Provider
import cv2
import logging
import numpy as np


class KLTTrajectoryDisplay(Display.Display):
    logger = logging.getLogger(__name__)

    def __init__(self, next_display):
        super().__init__(next_display)

    def _draw(self):
        for obj in self._objects:
            color = (255, 255, 255)
            # self.logger.debug("Trajectories: " + str(obj.trajectory))
            pt1 = np.array([obj.v0, obj.u0])
            # Draw kalman prediction
            if len(obj.trajectory) > 2:
                prediction = obj.predict_move()
                prediction = prediction.reshape(3)
                pt2 = prediction[:2]
                dz = prediction[2] - obj.distance
                l2 = np.sqrt(np.sum((pt2 - pt1) ** 2))
                length = 10 if l2 > 10 else l2
                dir = 5 * length * (pt2 - pt1) / l2 if l2 > 0.1 else 0.1
                cv2.arrowedLine(self._canvas, tuple(pt1), tuple((pt1 + dir).astype(int)), (184, 200, 130),
                                tipLength=0.5, thickness=4)
                dz = -2 if dz < -2 else dz
                dz = 0 if dz > 0 else dz
                try:
                    red = int(-dz * 128)
                except ValueError:
                    red = 0
                cv2.circle(self._canvas, (obj.v0, obj.u0 + obj.height), 7, (0, 0, red), -1)

