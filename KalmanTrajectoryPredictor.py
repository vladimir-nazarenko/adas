import TrajectoryPredictor
import numpy as np
import cv2


class KalmanTrajectoryPredictor(TrajectoryPredictor.TrajectoryPredictor):
    def __init__(self):
        self.klmn = cv2.KalmanFilter(6, 6, 0)
        self.klmn.measurementMatrix = np.eye(6, dtype=np.float32)
        self.klmn.transitionMatrix = np.array([[1, 0, 0, 1, 0, 0],
                                               [0, 1, 0, 0, 1, 0],
                                               [0, 0, 1, 0, 0, 1],
                                               [0, 0, 0, 1, 0, 0],
                                               [0, 0, 0, 0, 1, 0],
                                               [0, 0, 0, 0, 0, 1]], np.float32)
        self.klmn.processNoiseCov = np.eye(6, dtype=np.float32) * 1e-1

        self.pv = None
        self.pu = None
        self.pz = None

    def correct(self, point):
        v, u, z = point
        self.klmn.predict()
        if self.pv is None or self.pu is None:
            self.klmn.statePost = np.array([v, u, z, 0, 0, 0], dtype=np.float32).reshape((-1, 1))
        else:
            speed = (v - self.pv, u - self.pu, z - self.pz)
            self.klmn.correct(np.array([v, u, z, *speed], dtype=np.float32).reshape((-1, 1)))
        self.pv = v
        self.pu = u
        self.pz = z

    def predict(self):
        return self.klmn.predict()[:3, :]
