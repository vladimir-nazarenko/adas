import TrajectoryPredictor
import numpy as np
import cv2


class LineFittingTrajectoryPredictor(TrajectoryPredictor.TrajectoryPredictor):
    def __init__(self):
        self.history = np.zeros((0, 3))
        self.current_speed = np.array([0, 0, 0]).reshape(1, 3)
        self.predict_counter = 1

    def correct(self, point):
        mem = 5
        self.predict_counter = 1
        # z = point[2]
        # self.zs.insert(0, z)
        # point = point[:2]
        self.history = np.vstack([np.array(point).reshape((1, 3)), self.history])
        if len(self.history) >= mem:
            self.history = self.history[:-1, :]
        [ev, eu, v, u] = cv2.fitLine(self.history[:, :2], cv2.DIST_L2, 0, 0.01, 0.01)
        if self.history.shape[0] > 1:
            # on image plane
            differences = self.history[1:min(mem + 1, len(self.history))] - \
                          self.history[0:min(mem + 1, len(self.history)) - 1]
            speed = -differences[:, :2].dot(np.array([ev, eu]))
            avg_speed = np.array([ev, eu]) * np.median(speed)
            zspeed = (self.history[0, 2] - self.history[-1, 2]) / len(self.history) - 1
            avg_speed = np.append(avg_speed, zspeed)
        else:
            avg_speed = np.zeros((1, 3))
        self.current_speed = avg_speed


    def predict(self):
        prediction = self.history[0] + self.predict_counter * self.current_speed.reshape(-1)
        self.predict_counter += 1
        return prediction.astype(np.float32)
