import cv2
import glob
import resolver
import logging


class LostAndFoundImageLoader:
    logger = logging.getLogger(__name__)

    class State:
        def __init__(self, frame):
            self._frame = frame

    def __init__(self):
        directory = "01_Hanns_Klemm_Str_45"
        # TODO: FIX ME
        path_prefix = "/home/vnazarenko/data/lost_and_found/"
        left_dir = path_prefix + "leftImg8bit/train/" + directory
        right_dir = path_prefix + "rightImg8bit/train/" + directory
        self._names = list(zip(sorted(glob.glob(left_dir + "/*.png")), sorted(glob.glob(right_dir + "/*.png"))))
        self._current_frame = 0
        self._end_frame = len(self._names) - 1
        self._left = None
        self._right = None

    @staticmethod
    def not_initialized():
        raise RuntimeError("ImageLoader was not initialized. Call ImageLoader#next")

    def get_left(self):
        return self._left if self._left is not None else self.not_initialized()

    def get_right(self):
        return self._right if self._right is not None else self.not_initialized()

    def next(self):
        self._current_frame += 1
        self._left = cv2.imread(self._names[self._current_frame][0])
        assert self._left is not None, "Cannot read image"
        self._right = cv2.imread(self._names[self._current_frame][1])
        assert self._right is not None, "Cannot read image"
        print(self._names[self._current_frame][0])
        print(self._names[self._current_frame][1])
        self.logger.info("Loaded frame {0}".format(self._current_frame))

    def has_next(self):
        return self._current_frame < self._end_frame

    def get_current_frame_number(self):
        return self._current_frame

    def get_state(self):
        return self.State(self._current_frame)

    def is_in_state(self, state):
        return state._frame == self._current_frame
