import TrajectoryPredictor
import numpy as np


class MovingAverageTrajectoryPredictor(TrajectoryPredictor.TrajectoryPredictor):
    def __init__(self):
        self.history = np.zeros((0, 2))
        self.current_speed = np.array([0, 0]).reshape(1, 2)
        self.predict_counter = 1

    def correct(self, point):
        self.predict_counter = 1
        self.history = np.vstack([np.array(point).reshape((1, 2)), self.history])
        if self.history.shape[0] > 1:
            speed = self.history[1:min(6, len(self.history))] - self.history[0:min(6, len(self.history)) - 1]
            avg_speed = np.mean(speed)
        else:
            avg_speed = np.zeros((1, 2))
        self.current_speed = avg_speed

    def predict(self):
        prediction = self.history[0] + self.predict_counter * self.current_speed
        self.predict_counter += 1
        return prediction.astype(np.float32)
