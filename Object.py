import numpy as np
import warnings
import random
import cv2
import KalmanTrajectoryPredictor
import MovingAverageTrajectoryPredictor
import LineFittingTrajectoryPredictor

class Object:
    sequence = 0

    def __init__(self, u0=0, v0=0, width=0, height=0, provider=None, corners=None,
                 trajectory=None, distance=-1, pixels=None, class_name="???"):
        self.u0 = u0
        self.v0 = v0
        self.width = width
        self.height = height
        self.provider = provider
        self.corners = corners
        self.distance = distance
        self.pixels = pixels
        self.class_name = class_name
        # avoid mutable default argument
        self.trajectory = [] if trajectory is None else trajectory
        self.ind = self.sequence
        Object.sequence += 1
        # self.predictor = KalmanTrajectoryPredictor.KalmanTrajectoryPredictor()
        # self.predictor = MovingAverageTrajectoryPredictor.MovingAverageTrajectoryPredictor()
        self.predictor = LineFittingTrajectoryPredictor.LineFittingTrajectoryPredictor()

    def translated_corners(self, corners=None):
        pts = []
        if self.corners is None or corners.size == 0:
            warnings.warn(Warning("Interesting points were not set for {0}".format(repr(self))))
            return np.array([])
        else:
            if corners is None:
                corners = self.corners
            for p in corners:
                pts.append(np.array([p[0] + self.v0, p[1] + self.u0]))
            return np.vstack(pts).reshape((-1, 1, 2)).astype(np.float32)

    def move(self, dv, du, im_height, im_width):
        self.v0 = int(self.v0 + dv)
        self.u0 = int(self.u0 + du)
        status = self._check_borders(im_height, im_width)
        self.add_to_trajectory((self.v0, self.u0))
        return status

    # aligns borders with respect to image borders
    # returns True if object bounding box has intersection with the image
    # False otherwise
    def _check_borders(self, im_height, im_width):
        if self.v0 < 0:
            self.width += self.v0
            self.v0 = 0
        if self.u0 < 0:
            self.width += self.u0
            self.u0 = 0
        if self.v0 + self.width >= im_width:
            self.width = im_width - self.v0 - 1
        if self.u0 + self.height >= im_height:
            self.height = im_height - self.u0 - 1
        return 0 <= self.v0 < im_width and 0 <= self.u0 < im_height

    def back_translate_and_set_corners(self, pts):
        back_translated = []
        for p in pts:
            v, u = p[0]
            back_translated.append([v - self.v0, u - self.u0])
        self.corners = np.array(back_translated)

    def add_to_trajectory(self, point):
        v, u = point
        # TODO: Search here for problems when order of object provider chain is changed
        self.predictor.correct((v, u, self.distance))
        self.trajectory.append((v, u))

    def remove_last_point(self):
        if len(self.trajectory) > 0:
            self.trajectory.pop()

    def predict_move(self):
        return self.predictor.predict()
