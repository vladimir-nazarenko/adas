import Display
import cv2


class ObjectIdentifierDisplay(Display.Display):
    def __init__(self, next_display):
        super().__init__(next_display)

    def _draw(self):
        for obj in self._objects:
            v1 = obj.v0 + obj.width
            cv2.putText(self._canvas, str(obj.ind), (v1 - 20, obj.u0 + 15), cv2.FONT_HERSHEY_PLAIN,
                        1,
                        (1, 1, 1), thickness=2)
            cv2.putText(self._canvas, str(obj.ind), (v1 - 20, obj.u0 + 15), cv2.FONT_HERSHEY_PLAIN,
                        1,
                        (255, 255, 255), thickness=1)
