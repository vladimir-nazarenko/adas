
class ObjectSetSupplier:
    def __init__(self, supplier):
        self._supplier = supplier

    def get_object_set(self):
        object_set = self._supplier.get_object_set() if self._supplier is not None else []
        return self._process(object_set)

    def _process(self, object_set):
        raise NotImplementedError

    @staticmethod
    def _concat(obj_set_a, obj_set_b):
        return obj_set_a + obj_set_b
