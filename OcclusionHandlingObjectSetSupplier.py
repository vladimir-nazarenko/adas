from Provider import Provider
import ObjectSetSupplier
import logging
from KLTObjectSetSupplier import KLTObjectSetSupplier
from collections import defaultdict

class OcclusionHandlingObjectSetSupplier(ObjectSetSupplier.ObjectSetSupplier):
    logger = logging.getLogger(__name__)

    def __init__(self, supplier):
        super().__init__(supplier)

    def _process(self, object_set):
        # return self._remove_collision_of_yolo_and_klt(object_set)
        # print([obj.ind for obj in object_set])
        return \
            self._remove_repeated_ids(
            self._remove_collision_of_stixel_and_yolo(
                self._remove_collision_of_klt_and_klt(
                    self._remove_collision_of_yolo_and_klt(
                        self._remove_collision_of_stixel_and_stixel(object_set)))))

    @staticmethod
    def _intersection_score(obj1, obj2):
        # calculate width and height of the intersection
        w = min(obj1.v0 + obj1.width, obj2.v0 + obj2.width) - max(obj1.v0, obj2.v0)
        h = min(obj1.u0 + obj1.height, obj2.u0 + obj2.height) - max(obj1.u0, obj2.u0)
        intersection_area = w * h if w > 0 and h > 0 else 0
        intersection_score = min(obj2.width * obj2.height, obj1.width * obj1.height) / \
                             intersection_area if intersection_area > 0 else 0
        return intersection_score

    @staticmethod
    def _intersection_over_union(obj1, obj2):
        # calculate width and height of the intersection
        w = min(obj1.v0 + obj1.width, obj2.v0 + obj2.width) - max(obj1.v0, obj2.v0)
        h = min(obj1.u0 + obj1.height, obj2.u0 + obj2.height) - max(obj1.u0, obj2.u0)
        intersection_area = w * h if w > 0 and h > 0 else 0
        union_area = obj1.width * obj1.height + obj2.width * obj2.height - intersection_area
        intersection_score = intersection_area / union_area
        return intersection_score

    def _remove_repeated_ids(self, object_set):
        ids = defaultdict(list)
        filtered = []
        for obj in object_set:
            ids[obj.ind].append(obj)
        for k, v in ids.items():
            klts = list(filter(lambda x: x.provider == Provider.KLT, v))
            if len(klts) > 0:
                filtered.append(klts[0])
            else:
                filtered.append(v[0])
        return filtered

    def _remove_collision_of_yolo_and_klt(self, object_set):
        bad_index = set()
        for i, klt_object in enumerate(object_set):
            for j, yolo_object in enumerate(object_set):
                if klt_object.provider == Provider.KLT and yolo_object.provider == Provider.YOLO:
                    if self._intersection_over_union(yolo_object, klt_object) > 0.2 and\
                                    klt_object.class_name == yolo_object.class_name:
                        bad_index.add(i)
                        self.logger.debug("Removed object tracked by klt with id {0} and class name {1}".format(
                            klt_object.ind, klt_object.class_name))
                        yolo_object.trajectory = klt_object.trajectory
                        yolo_object.predictor = klt_object.predictor
                        yolo_object.ind = klt_object.ind
                        KLTObjectSetSupplier.update_tracked_to_be_as_detected(klt_object, yolo_object)
                        if klt_object.ttl > 1:
                            KLTObjectSetSupplier.invalidate(yolo_object)
                        # klt_object.provider = Provider.YOLO
        if len(bad_index) > 0:
            self.logger.debug("Removed {0} objects tracked by klt".format(len(bad_index)))

        return [x for i, x in enumerate(object_set) if i not in bad_index]
        # return object_set

    def _remove_collision_of_klt_and_klt(self, object_set):
        bad_index = set()
        for i1, obj1 in enumerate(object_set):
            for i2, obj2 in list(enumerate(object_set))[i1+1:]:
                if obj1.provider == Provider.KLT and obj2.provider == Provider.KLT:
                    if self._intersection_over_union(obj2, obj1) > 0.3 and\
                                    obj1.class_name == obj2.class_name:
                        if obj1.ttl > obj2.ttl:
                            bad_index.add(i2)
                            KLTObjectSetSupplier.invalidate(obj2)
                        else:
                            bad_index.add(i1)
                            KLTObjectSetSupplier.invalidate(obj1)
        if len(bad_index) > 0:
            self.logger.info("Removed {0} objects tracked by klt".format(len(bad_index)))
        # return object_set
        return [x for i, x in enumerate(object_set) if i not in bad_index]

    def _remove_collision_of_stixel_and_yolo(self, object_set):
        bad_index = set()
        for i, stixel_object in enumerate(object_set):
            for obj in object_set:
                if stixel_object.provider == Provider.STIXEL and\
                        (obj.provider == Provider.YOLO or obj.provider == Provider.KLT):
                    if self._intersection_over_union(obj, stixel_object) > 0.3 or \
                                    self._intersection_score(obj, stixel_object) > 0.99:
                            # (self._intersection_score(obj, stixel_object) > 0.99 and
                            #  0 < stixel_object.distance - obj.distance < 1):
                        bad_index.add(i)
        if len(bad_index) > 0:
            self.logger.info("Removed {0} stixels objects intersecting yolo objects".format(len(bad_index)))

        return [x for i, x in enumerate(object_set) if i not in bad_index]

    def _remove_collision_of_stixel_and_stixel(self, object_set):
        bad_index = set()
        for i, obj1 in enumerate(object_set):
            for obj2 in object_set:
                if obj1.provider == Provider.STIXEL and obj2.provider == Provider.STIXEL:
                    if self._intersection_score(obj1, obj2) > 0.99:
                        if obj1.height * obj1.width < obj2.height * obj2.width:
                            bad_index.add(i)
        if len(bad_index) > 0:
            self.logger.info("Removed {0} stixels objects intersecting stixel objects".format(len(bad_index)))

        return [x for i, x in enumerate(object_set) if i not in bad_index]
