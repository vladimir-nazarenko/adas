from enum import Enum


class Provider(Enum):
    YOLO = 1
    STIXEL = 2
    KLT = 3