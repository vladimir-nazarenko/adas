# README #

### This project demonstrates object detection and tracking on the moving platform (vehicle) using something like Stixels (http://rodrigob.github.io/documents/2011_iccv_cvvt_workshop_stixels_estimation.pdf) and KLT tracker. Currently we maintain just the in-city situations. ###

This is only research quality code written in rush, so expect comments rarely.

To get things work, you would want to 

* install python 3

* install opencv 3 with python 3 bindings

* download the dataset you like (kitti, lost and found, cityscapes)

* possibly, provide some reference detection (we used yolo detections (https://pjreddie.com/darknet/yolo/))


##Author:
Vladimir Nazarenko (nazarenko2010@gmail.com)