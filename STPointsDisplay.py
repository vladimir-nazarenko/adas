import Display
import logging
import cv2
import math


class STPointsDisplay(Display.Display):
    logger = logging.getLogger(__name__)

    def __init__(self, next_display):
        super().__init__(next_display)

    def _draw(self):
        for obj in self._objects:
            if obj.corners is not None:
                for v, u in obj.corners:
                    v = int(v)
                    u = int(u)
                    cv2.drawMarker(self._canvas, (int(v + obj.v0), int(u + obj.u0)), (100, 30, 140))
                    label = "{0:.2f}".format(obj.distance) if not math.isinf(obj.distance) else "inf"
                    # cv2.putText(self._canvas, label, (obj.v0 + v, obj.u0 + u), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                    #             1 / 2,
                    #             (1, 1, 1), thickness=5)
                    # cv2.putText(self._canvas, label, (obj.v0 + v, obj.u0 + u), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                    #             1 / 2,
                    #             (255, 255, 255), thickness=2)
        # TODO: REMOVE
        print()
