import Display
import cv2
import math
import resolver


class SaveTrackingDisplay(Display.Display):
    def __init__(self, next_display):
        super().__init__(next_display)
        self._out = open("tracking.txt", "a")

    def _draw(self):
        pass

    def _show_telemetry(self):
        for obj in self._objects:
            self._out.write("{0} {1} {2} -1 -1 -1 {3} {4} {5} {6} -1 -1 -1 -1 -1 -1 -1\n".format(
                resolver.image_loader.get_current_frame_number() - 1,
                obj.ind,
                obj.class_name,
                obj.v0,
                obj.u0,
                obj.v0 + obj.width,
                obj.u0 + obj.height
            ))

    def __del__(self):
        self._out.close()
