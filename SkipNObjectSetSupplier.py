import ObjectSetSupplier


class SkipNObjectSetSupplier(ObjectSetSupplier.ObjectSetSupplier):
    def __init__(self, n, supplier):
        super().__init__(supplier)
        self._N = n
        self._cnt = n

    def get_object_set(self):
        if self._cnt % self._N == 0:
            objects = super().get_object_set()
            self._cnt = 1
        else:
            objects = []
            self._cnt += 1
        return objects

    def _process(self, object_set):
        return object_set
