import pykitti
import resolver
import logging

# TODO: refactor me
class SpeedLoader:
    logger = logging.getLogger(__name__)

    def __init__(self, image_loader):
        self._image_loader = image_loader
        self._loader_state = None
        self._speed = None
        self._config = resolver.config
        self._data = pykitti.raw("", "/home/vnazarenko/kitti/2011_09_29/2011_09_29", "0071")
        self._data.load_oxts()
        self._counter = 0
        # TODO: refactor
        self._roll = 0

    def _compute_and_set_speed(self):
        self._speed = self._data.oxts[self._counter].packet.vf
        self._counter += 1

    def get(self):
        # if needed, reset
        if self._loader_state is None or not self._image_loader.is_in_state(self._loader_state):
            self._loader_state = self._image_loader.get_state()
            self._compute_and_set_speed()

            # TODO: refactor
            self._roll = self._data.oxts[self._counter].packet.roll
        return self._speed

    def get_roll(self):
        return self._roll

    def get_state(self):
        return self._loader_state

    def is_in_state(self, state):
        return self._image_loader.is_in_state(state)
