import Display
import resolver

import cv2
import numpy as np


class StereoDebugDisplay(Display.Display):
    def __init__(self, next_display):
        super().__init__(next_display)

    def _draw(self):
        pass

    def _show_telemetry(self):
        cv2.imshow("Disparity", resolver.disparity_loader.get().astype(np.uint8))
        cv2.imshow("Distance map", resolver.distance_loader.get().astype(np.uint8))
