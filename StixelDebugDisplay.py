import Display
import cv2
import resolver


class StixelDebugDisplay(Display.Display):
    def __init__(self, stixel_estimator, next_display):
        super().__init__(next_display)
        self._stixel_estimator = stixel_estimator

    def _draw(self):
        pass

    def _show_telemetry(self):
        cv2.imshow("Ground roi", self._stixel_estimator.ground_roi)
        cv2.imshow("Stixel roi", self._stixel_estimator.stixel_roi)
        left = resolver.image_loader.get_left().copy()
        horizon = self._stixel_estimator._horizon
        cv2.line(left, (0, horizon), (left.shape[1] - 1, horizon), (255, 0, 0), 3)
        cv2.imshow("Stixels", cv2.addWeighted(
            cv2.cvtColor(self._stixel_estimator.stixels, cv2.COLOR_GRAY2BGR), 1/3, left, 2/3, 0))

