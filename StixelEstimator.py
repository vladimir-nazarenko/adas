import cv2
import resolver

import numpy as np
import time
import logging


class StixelEstimator:
    logger = logging.getLogger(__name__)

    _disparity = None
    _horizon = None
    _ground_line_tan = None
    ground_roi = None
    stixel_roi = None
    stixels = None

    def __init__(self, horizon_estimator):
        self._config = resolver.config
        self._horizon_estimator = horizon_estimator
        self._horizon_estimator_state = None

    def set_ground_roi(self):
        roi = np.zeros_like(self._disparity)
        im_shape = roi.shape
        im_center_v = self._disparity.shape[1] // 2
        left_bottom = im_center_v - int(self._config["left_bottom"])
        right_bottom = im_center_v + int(self._config["right_bottom"])
        left_top = im_center_v - int(self._config["left_top"])
        right_top = im_center_v + int(self._config["right_top"])
        disparity_offset = int(self._config["disparity_offset"])

        points = np.array([[im_shape[0] - 1, left_bottom], [self._horizon, left_top],
                           [self._horizon, right_top], [im_shape[0] - 1, right_bottom]])

        # flip coordinates from (u, v) to (v, u) for opencv
        points[:, [0, 1]] = points[:, [1, 0]]

        cv2.fillConvexPoly(roi, points, 255)
        roi[:, 0:disparity_offset] = 0

        self.ground_roi = roi

    def get_stixel_roi(self):
        cot = 1 / self._ground_line_tan

        roll = float(self._config["roll"])
        # resolver.speed_loader.get()
        # roll = resolver.speed_loader.get_roll()
        # roll /= 180
        print("ROLL", roll)
        road_matching = int(self._config["road_matching"])
        roll_tan = np.tan(roll)
        im_height = self._disparity.shape[0]
        im_width = self._disparity.shape[1]

        stixel_roi = self._disparity.copy()
        expected_disparity = cot * (np.arange(self._horizon, im_height) - self._horizon)
        expected_disparity = np.vstack([np.zeros((im_height - len(expected_disparity), 1), dtype=np.uint8),
                                        expected_disparity[:, np.newaxis]])

        roll_delta = np.arange(0, im_width, dtype=float) - im_width / 2
        roll_delta *= roll_tan
        expected_disparity = expected_disparity + expected_disparity * roll_delta

        stixel_roi[self._disparity - expected_disparity <= road_matching] = 0

        self.stixel_roi = stixel_roi

    def compute_stixel_height(self):
        max_obstacle_height = int(self._config["max_obstacle_height"])
        min_obstacle_height = int(self._config["min_obstacle_height"])
        max_disparity_difference_in_stixel = int(self._config["max_disparity_difference_in_stixel"])
        stixel_above_ground = int(self._config["stixel_above_ground"])

        # where we want to look for the objects
        ground_roi = self.ground_roi
        # where do we have suspicious pixels
        stixel_roi = self.stixel_roi.copy()
        im_height = self._disparity.shape[0]
        disparity = self._disparity
        stixels = np.zeros_like(stixel_roi)

        for u, row in reversed(list(enumerate(ground_roi))[self._horizon:]):
            scale = (u - self._horizon) / (im_height - self._horizon)
            obstacles = np.where(np.logical_and(stixel_roi[u, :] > 0, row > 0))
            for v in obstacles[0]:
                init_disparity = int(disparity[u, v])
                stixel = u
                while u - stixel < max_obstacle_height * scale and stixel_roi[stixel, v] > 0 and \
                      disparity[stixel, v] > 0 and \
                      abs(int(disparity[stixel, v]) - init_disparity) < max_disparity_difference_in_stixel:
                    stixel_roi[stixel, v] = 0
                    stixel -= 1

                if u - stixel > min_obstacle_height * scale:
                    for y in range(stixel, min(u + stixel_above_ground, len(ground_roi) - 1)):
                        stixels[y, v] = 255
        self.stixels = stixels

    def compute_stixels(self):
        if self._horizon_estimator_state is None or not self._horizon_estimator.is_in_state(
                self._horizon_estimator_state):
            self._horizon_estimator.estimate_horizon()
            self._disparity = self._horizon_estimator.disparity
            self._horizon = self._horizon_estimator.horizon
            self._ground_line_tan = self._horizon_estimator.tan
            self._horizon_estimator_state = self._horizon_estimator.get_state()

            start = time.time()
            self.set_ground_roi()
            self.get_stixel_roi()
            self.compute_stixel_height()
            self.logger.info("Recalculated stixels in {0:.0f} ms".format(1000 * (time.time() - start)))

    def get_state(self):
        return self._horizon_estimator_state

    def is_in_state(self, state):
        return self._horizon_estimator_state.is_in_state(state)
