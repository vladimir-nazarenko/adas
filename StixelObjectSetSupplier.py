import ObjectSetSupplier
import Object
from Provider import Provider
import cv2
import numpy as np
import resolver


class StixelObjectSetSupplier(ObjectSetSupplier.ObjectSetSupplier):
    def __init__(self, stixel_estimator, disparity_loader, supplier):
        super().__init__(supplier)
        self._stixel_estimator = stixel_estimator
        # self._disparity_loader = disparity_loader

    def _process(self, object_set):
        return self._concat(object_set, self._get_stixel_objects())

    def _get_stixel_objects(self):
        self._stixel_estimator.compute_stixels()
        stixels = self._stixel_estimator.stixels.copy()
        self._disparity = self._stixel_estimator._disparity
        components = self._extract_connected_components(stixels)
        objects = self._components2objects(components)
        # img = self._disparity
        # img[stixels == 0] = 0
        # cv2.imshow("stix", stixels)
        return objects

    # Separates stixel roi into connected components, in each component performs background subtraction using
    # disparity and resegments image with subtracted background
    def _extract_connected_components(self, stixels):
        distance = resolver.distance_loader.get()
        # img = distance_map.astype(np.uint8)
        # img[stixels == 0] = 0
        # img = cv2.Canny(img, 5, 10)
        # cv2.imshow("cc", img)
        stixels[distance > 20] = 0
        stixels = cv2.morphologyEx(stixels, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5)))
        # cv2.imshow("cc", stixels)
        num_labels, label = cv2.connectedComponents(stixels)
        for i in range(1, num_labels):
            segment = np.where(label == i)
            if len(segment[0]) < 400:
                stixels[label == i] = 0
            else:
                disparity = self._disparity
                if np.std(distance[label == i]) > 1:
                    low = np.percentile(distance[label == i], 5)
                    high = np.percentile(distance[label == i], 20)
                    highest = np.percentile(distance[label == i], 80)
                    stixels[np.logical_and(distance < low, label == i)] = 0
                    stixels[np.logical_and(distance > highest, label == i)] = 0
                    label[np.logical_and(distance < low, label == i)] = 0
        good_components = []
        stixels = cv2.morphologyEx(stixels, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
        num_labels, label = cv2.connectedComponents(stixels)
        for i in range(1, num_labels):
            segment = np.where(label == i)
            if len(segment[0]) < 400:
                stixels[label == i] = 0
                label[label == i] = 0
            else:
                good_components.append(i)
        return label, good_components

    def _components2objects(self, components):
        labels_for_pixel, labels = components
        objects = []
        for label in labels:
            pixels = np.array(np.where(labels_for_pixel == label))
            u, v, h, w = cv2.boundingRect(pixels.T)
            obj = Object.Object(u0=u, v0=v, width=w, height=h, provider=Provider.STIXEL)
            objects.append(obj)
        return objects
