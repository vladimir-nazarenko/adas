import Display
import cv2
import math


class TTCDisplay(Display.Display):
    def __init__(self, next_display):
        super().__init__(next_display)

    def _draw(self):
        for obj in self._objects:
            try:
                label = "{0:.1f}".format(obj.ttc) if not math.isinf(obj.distance) else "inf"
                cv2.putText(self._canvas, label, (obj.v0 + obj.width - 20, obj.u0 - 5), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                            1 / 2,
                            (1, 1, 1), thickness=5)
                cv2.putText(self._canvas, label, (obj.v0 + obj.width - 20, obj.u0 - 5), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                            1 / 2,
                            (255, 255, 255), thickness=2)
            except AttributeError:
                # print("No such attribute")
                pass
