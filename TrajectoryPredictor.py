

class TrajectoryPredictor:
    def correct(self, point):
        raise NotImplementedError()

    def predict(self):
        raise NotImplementedError()
