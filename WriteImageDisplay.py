import Display
import cv2
import resolver
import os
import logging


class WriteImageDisplay(Display.Display):
    logger = logging.getLogger(__name__)

    def __init__(self, show, next_display):
        super().__init__(next_display)
        self._show_img = show
        self._output_dir = "output/" + resolver.config["drive"] + "/"
        if not os.path.exists(self._output_dir):
            os.makedirs(self._output_dir)
        self.logger.info("Writing images into {0}".format(self._output_dir))

    def _draw(self):
        pass

    def _show_telemetry(self):
        frame = resolver.image_loader.get_current_frame_number()
        path = self._output_dir + "{0:010d}.png".format(frame)
        cv2.imwrite(path, self._canvas)
        self.logger.debug("Wrote image {0}".format(path))

    def _show(self):
        if self._show_img:
            super()._show()
