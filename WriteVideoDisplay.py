import Display
import cv2
import resolver


class WriteVideoDisplay(Display.Display):
    def __init__(self, show, fps, next_display):
        super().__init__(next_display)
        fourcc = cv2.VideoWriter_fourcc(*"XVID")
        self._shape = (1300, 400)
        self._video = cv2.VideoWriter(resolver.config["output_file"], fourcc, fps, self._shape)
        self._show_img = show

    def _draw(self):
        pass

    def _show_telemetry(self):
        img = cv2.resize(self._canvas, self._shape)
        self._video.write(img)

    def _show(self):
        if self._show_img:
            super()._show()
