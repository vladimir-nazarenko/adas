import ObjectSetSupplier
import Object
import resolver

from Provider import Provider
import logging


class YoloObjectSetSupplier(ObjectSetSupplier.ObjectSetSupplier):
    logger = logging.getLogger(__name__)

    def __init__(self, supplier, image_loader):
        super().__init__(supplier)
        self._image_loader = image_loader
        self._classes = self._load_yolo_classes()

    def _process(self, object_set):
        return self._concat(object_set, self._get_yolo_objects())

    def _get_yolo_objects(self):
        objects = []
        for box in self._load_yolo_boxes():
            v0, u0, v1, u1, class_id = box
            if class_id in [-1, 0, 1, 2, 3, 5, 7, 13, 16, 56, 58, 60]:
                obj = Object.Object(u0=u0, v0=v0, width=v1-v0, height=u1-u0, class_name=self._classes[class_id],
                                    provider=Provider.YOLO)
                objects.append(obj)
        return self.filter_occlusions(objects)

    @staticmethod
    def _intersection_over_union(obj1, obj2):
        # calculate width and height of the intersection
        w = min(obj1.v0 + obj1.width, obj2.v0 + obj2.width) - max(obj1.v0, obj2.v0)
        h = min(obj1.u0 + obj1.height, obj2.u0 + obj2.height) - max(obj1.u0, obj2.u0)
        intersection_area = w * h if w > 0 and h > 0 else 0
        union_area = obj1.width * obj1.height + obj2.width * obj2.height - intersection_area
        intersection_score = intersection_area / union_area
        return intersection_score

    def filter_occlusions(self, object_set):
        bad_index = set()
        for i1, obj1 in enumerate(object_set):
            for i2, obj2 in list(enumerate(object_set))[i1 + 1:]:
                if self._intersection_over_union(obj2, obj1) > 0.3 and \
                                obj1.class_name == obj2.class_name:
                    if obj1.width * obj1.height < obj2.width * obj2.height:
                        bad_index.add(i2)
                        self.logger.warn(
                            "Removed object clashed with object with id {0} and label {1}".format(
                                obj1.ind, obj1.class_name))
                    else:
                        bad_index.add(i1)
                        self.logger.warn(
                            "Removed object clashed with object with id {0} and label {1}".format(
                                obj2.ind, obj2.class_name))
        return [x for i, x in enumerate(object_set) if i not in bad_index]

    @staticmethod
    def _load_yolo_classes():
        config = resolver.config
        path = config["classes"]
        with open(path, "r") as cl:
            classes = {}
            for l in cl.readlines():
                k, v = tuple(l.split(" ")[0:2])
                k = int(k)
                if k in [1, 3]:
                    classes[k] = "bike"
                elif k in [2, 5, 7]:
                    classes[k] = "Car"
                elif k in [13, 16, 56, 58, 60]:
                    classes[k] = "misc"
                else:
                    classes[k] = v.strip()
            return classes

    def _load_yolo_boxes(self):
        config = resolver.config
        path = config["kitti"] + config["boxes"]
        boxes = []
        frame = self._image_loader._current_frame
        with open("{0}/{1:010d}.txt".format(path, frame), "r") as f:
            for l in f.readlines():
                numbers = [int(x) for x in l.split(" ")]
                boxes.append(numbers)
        return boxes
