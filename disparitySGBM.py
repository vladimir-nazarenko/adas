import argparse
import cv2
import numpy as np


def disparity_defaults():
    return {"disparity_wsize": 5, "disparity_max": 128, "disparity_p1": 32, "disparity_p2": 80,
            "disparity_prefiltercap": 63, "disparity_kernel1": 20, "disparity_kernel2": 3}


# computes disparity between two images using stereoSGBM algorithm and WLS filter
def get_disparity_sgbm(left, right, disparity_config=None):
    # setting parameters
    wsize = int(disparity_config["disparity_wsize"])
    max_disp = int(disparity_config["disparity_max"])
    p1 = int(disparity_config["disparity_p1"])
    p2 = int(disparity_config["disparity_p2"])
    prefiltercap = int(disparity_config["disparity_prefiltercap"])
    kernel_size1 = int(disparity_config["disparity_kernel1"])
    kernel_size2 = int(disparity_config["disparity_kernel2"])

    left_matcher = cv2.StereoSGBM_create(0, int(max_disp), wsize)
    left_matcher.setP1(p1 * wsize * wsize)
    left_matcher.setP2(p2 * wsize * wsize)
    left_matcher.setPreFilterCap(prefiltercap)
    left_matcher.setMode(cv2.STEREO_SGBM_MODE_SGBM_3WAY)
    wls_filter = cv2.ximgproc.createDisparityWLSFilter(left_matcher)
    right_matcher = cv2.ximgproc.createRightMatcher(left_matcher)
    # computing disparities
    left_disparity = left_matcher.compute(left, right)
    right_disparity = right_matcher.compute(right, left)
    # filtering the result
    filtered = wls_filter.filter(left_disparity, left, None, right_disparity)
    # remove negative disparities, if present
    filtered[filtered < 0] = filtered.max()
    # getting true disparity values
    disparity = filtered / 16
    disparity = disparity.astype(np.uint8)
    # smoothing to fill holes
    smooth = cv2.morphologyEx(disparity,
                              cv2.MORPH_CLOSE,
                              cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernel_size1, kernel_size1)))
    # filling holes
    disparity[disparity == 0] = smooth[disparity == 0]
    # filling smaller holes where disparity is not zero
    disparity = cv2.morphologyEx(disparity,
                                 cv2.MORPH_CLOSE,
                                 cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernel_size2, kernel_size2)))
    return disparity


# Reads command line arguments
def get_arguments():
    parser = argparse.ArgumentParser(description="Compute disparity between !rectified! images.")
    parser.add_argument("left", type=str, nargs=1, help="Path to left image")
    parser.add_argument("right", type=str, nargs=1, help="path to right image")
    parser.add_argument("--output", type=str, nargs=1, help="path to output image", default="disparity.png")
    args = parser.parse_args()
    args = vars(args)
    for k, v in args.items():
        args[k] = v[0]
    return args


def main():
    arguments = get_arguments()
    left = cv2.imread(arguments["left"], cv2.IMREAD_GRAYSCALE)
    right = cv2.imread(arguments["right"], cv2.IMREAD_GRAYSCALE)
    assert left is not None, "invalid left image: {0}".format(arguments["left"])
    assert right is not None, "invalid right image: {0}".format(arguments["right"])
    disparity = get_disparity_sgbm(left, right, disparity_defaults())
    output_path = arguments["output"] if "output" in arguments else "disparity.png"
    print("Wrote into {0}".format(output_path))
    cv2.imwrite(output_path, disparity)

# If executed as the main script, computes the disparity between two images,
# given their names
if __name__ == "__main__":
    main()

