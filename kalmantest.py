import numpy as np
import cv2
import matplotlib.pyplot as plt
import pandas as pd
import scipy.signal as signal


data40 = {88: [(750, 168), (753, 168), (754, 171), (754, 169), (753, 168), (755, 162), (754, 161), (754, 162), (752, 164), (749, 166), (747, 163), (746, 168), (748, 170), (751, 169), (754, 170), (763, 162), (768, 161), (769, 162), (773, 164), (780, 163), (783, 162), (781, 162), (789, 164), (789, 167), (789, 168), (794, 161), (803, 160), (808, 159), (810, 159), (818, 160), (824, 161), (828, 165), (837, 153), (841, 158), (851, 161), (857, 160), (869, 158), (884, 163), (892, 163), (908, 162), (929, 160), (943, 159), (935, 156), (941, 158), (976, 162), (955, 120), (1004, 120), (1058, 156), (1129, 79), (1089, 150)],
          959: [(758, 167), (758, 167), (753, 161), (749, 157), (749, 159), (746, 160), (743, 161), (744, 162), (742, 161), (741, 162), (739, 164), (739, 166), (734, 166), (731, 167), (733, 168), (732, 169), (729, 170), (727, 170), (729, 171), (729, 170), (729, 170), (730, 168), (729, 168), (732, 168), (730, 170), (738, 177), (740, 175), (744, 174), (741, 176), (746, 174), (757, 172), (770, 171), (774, 170), (778, 171), (787, 171), (788, 177), (792, 190), (812, 173), (821, 167), (827, 169), (837, 173), (847, 172), (855, 172), (866, 164), (871, 169), (885, 184), (894, 175), (909, 169), (918, 169), (912, 170), (913, 172), (916, 185), (941, 178), (948, 175), (969, 179), (984, 176), (977, 166), (993, 162), (999, 168), (1026, 159), (1044, 168), (1082, 161), (1110, 161), (1115, 154), (1124, 165), (1130, 161), (1181, 168)],
          764: [(664, 146), (665, 146), (660, 141), (656, 142), (654, 142), (652, 142), (649, 142), (645, 141), (642, 141), (638, 138), (638, 142), (630, 111), (632, 143), (630, 142), (627, 107), (626, 110), (628, 138), (627, 138), (626, 133), (623, 130), (619, 130), (625, 136), (622, 136), (619, 137), (614, 141), (614, 142), (612, 144), (609, 144), (612, 137), (612, 141), (612, 137), (612, 142), (612, 141), (612, 145), (611, 146), (612, 143), (612, 144), (612, 144), (612, 169), (614, 169), (612, 141), (616, 141), (612, 169), (616, 169), (620, 169), (623, 133), (621, 133), (622, 133), (627, 133), (626, 136), (634, 130), (638, 138), (640, 138), (644, 39), (650, 38), (671, 134), (675, 137), (683, 136), (670, 44), (674, 99), (681, 98), (687, 97), (695, 27), (688, 97), (692, 31), (692, 93), (693, 95), (691, 26), (697, 85), (700, 84), (704, 92), (705, 84), (710, 87), (711, 84), (720, 79), (729, 71), (735, 71), (742, 75), (729, 53), (732, 43), (727, 39), (754, 37), (753, 30), (764, 28), (743, 27), (738, 20), (734, 8), (747, 26), (744, 31), (745, 17), (760, 16), (758, 0), (760, 11), (767, 11), (750, 0), (740, 0), (745, 0), (746, 0), (738, 0), (747, 1), (784, 54), (817, 56), (747, 49), (751, 51), (756, 54)],
          2178: [(685, 213), (687, 211), (688, 207), (697, 198), (687, 185), (688, 185), (689, 184), (689, 188), (690, 186), (692, 188), (692, 187), (696, 185), (699, 185), (701, 185), (702, 200), (705, 201), (707, 200), (707, 201), (709, 202), (709, 203), (712, 205), (709, 206), (712, 206), (726, 204), (722, 205), (728, 215), (723, 216), (730, 219), (730, 219), (734, 222), (759, 217), (752, 229), (755, 228), (760, 232), (773, 237), (756, 240), (777, 243), (785, 246), (808, 252), (807, 271), (823, 271), (850, 281), (861, 292), (865, 276), (900, 286)]
          }


for k in data40.keys():
    img = np.zeros((400, 1300), dtype=np.uint8)
    [vx, vy, x, y] = cv2.fitLine(np.array(data40[k]), cv2.DIST_L12, 0, 0.01, 0.01)
    for pt1, pt2 in zip(data40[k][:-1], data40[k][1:]):
        cv2.arrowedLine(img, pt1, pt2, 255)
        dp = [pt2[i] - pt1[i] for i in range(len(pt1))]
        print(vx * dp[0] + vy * dp[1])
    print()
    # speed = np.array()

    lefty = int((-x * vy / vx) + y)
    righty = int(((1300 - x) * vy / vx) + y)
    cv2.line(img, (1300 - 1, righty), (0, lefty), 100, 2)
    # l = cv2.fitLine(np.array(data40[k]), cv2.DIST_FAIR, 0, 0.01, 0.01)
    # print(l)
    # print(np.polyfit(np.array(data40[k])[:, 0], np.array(data40[k])[:, 1], 1))
    # cv2.arrowedLine(img, tuple(l[:2, :]), tuple(l[2:, :]), 255)
    cv2.imshow(str(k), img)
cv2.waitKey(0)

exit()


def ema(seq, a=0.4):
    assert len(seq.shape) == 2
    emas = np.zeros(shape=(0, seq.shape[1]), dtype=seq.dtype)
    if len(seq) > 0:
        emas = np.append(emas, seq[0, np.newaxis], axis=0)
    for s in seq[1:]:
        ema = s * a + emas[-1] * (1 - a)
        emas = np.append(emas, ema[np.newaxis], axis=0)
    return emas


data = [[(761, 181), (758, 169), (760, 166), (757, 168), (754, 171), (752, 173), (749, 170), (750, 168), (753, 168), (754, 171), (754, 169), (753, 168), (755, 162), (754, 161), (754, 162), (752, 164), (749, 166), (747, 163), (746, 168), (748, 170), (751, 169), (754, 170), (763, 162), (768, 161), (769, 162), (773, 164), (780, 163), (784, 162), (781, 162), (789, 164), (789, 167), (789, 168), (794, 161), (803, 160), (808, 159), (810, 159), (818, 160), (824, 161), (828, 165), (837, 153), (841, 158), (851, 161), (857, 160), (869, 158)],
[(437, 150), (442, 172), (441, 171), (442, 171), (435, 173), (429, 166), (423, 167), (423, 164), (417, 162), (402, 164), (398, 161), (391, 160), (378, 162), (368, 167), (373, 166), (338, 169), (321, 170)],
[(387, 154), (388, 154), (388, 149), (388, 147), (382, 152), (383, 147), (385, 148), (382, 146), (378, 145), (380, 145), (371, 175), (369, 174), (368, 175), (366, 174), (366, 174), (351, 174), (354, 168), (351, 164), (351, 163), (345, 160), (339, 163), (339, 162), (327, 161), (319, 158), (311, 157), (288, 158), (266, 170), (247, 176), (261, 171), (219, 168), (165, 153), (143, 155), (97, 158), (7, 159)],
[(452, 192), (449, 188), (450, 186), (450, 185), (448, 184), (449, 184), (448, 184), (451, 180), (452, 177), (451, 181), (452, 180), (453, 178), (452, 178), (451, 179), (452, 184), (450, 186), (448, 187), (445, 188), (440, 190), (420, 201), (414, 203), (419, 203), (416, 205), (409, 204), (392, 199), (379, 220), (372, 221), (362, 228), (365, 226), (358, 231), (332, 241), (320, 248)]]





data = [np.array([list(x) for x in xs]) for xs in data]
id = 3
seq = np.array(data[id])
meanseq = pd.rolling_mean(seq, 7)
fig, axes = plt.subplots(3, 2)
axes[0, 0].plot(seq[:, 0], 'b')
axes[0, 0].plot(meanseq[:, 0], 'b')
axes[0, 1].plot(seq[:, 1], 'g')
axes[0, 1].plot(meanseq[:, 1], 'g')
speed = seq[1:, :] - seq[:-1, :]
# meanspeed = meanseq[1:, :] - meanseq[:-1, :]
meanspeed = pd.rolling_mean(speed, 7)
axes[1, 0].plot(speed)
axes[1, 1].plot(meanspeed)
plt.show()


exit()
emaseq = ema(seq)
speed = seq[1:, :] - seq[:-1, :]
acc = speed[1:, :] - speed[:-1, :]
# emaspeed = emaseq[1:, :] - emaseq[:-1, :]
# emaacc = emaspeed[1:, :] - emaspeed[:-1, :]
emaspeed = ema(speed, 0.2)
emaacc = ema(acc, 0.2)
# fig, axes = plt.subplots(3, 2)
# axes[0, 0].plot(seq[:, 0], 'b')
# axes[0, 0].plot(emaseq[:, 0], 'b')
# axes[0, 1].plot(seq[:, 1], 'g')
# axes[0, 1].plot(emaseq[:, 1], 'g')
# axes[1, 0].plot(speed)
# axes[2, 0].plot(acc)
# axes[1, 1].plot(emaspeed)
# axes[2, 1].plot(emaacc)
# plt.show()
# ema(seq)
exit()
plt.ion()
fig, axes = plt.subplots(1, 2)
plt.show()
for i in range(2, len(seq) - 2):
    axes[0].clear()
    axes[1].clear()
    split = i
    # print(np.tile(np.arange(20).reshape((-1, 1)), (1, 2)) * speed[split - 1])
    prediction = emaseq[:split, :]
    # prediction = np.vstack(
    #     [prediction, prediction[split - 1, np.newaxis] + np.tile(np.arange(20).reshape((-1, 1)), (1, 2)) *
    #      emaspeed[split] + np.tile((np.arange(20) ** 2).reshape((-1, 1)), (1, 2)) * emaacc[split] / 2])
    prediction = np.vstack(
        [prediction, prediction[split - 1, np.newaxis] + np.tile(np.arange(20).reshape((-1, 1)), (1, 2)) *
         emaspeed[split]])
    axes[0].plot(seq[:, 0], 'b')
    axes[1].plot(seq[:, 1], 'g')
    axes[0].plot(prediction[:, 0], 'r')
    axes[1].plot(prediction[:, 1], 'r')
    fig.canvas.draw()
    plt.pause(0.5)
plt.waitforbuttonpress()
exit()
cv2.namedWindow("seq")

img = np.zeros((500, 1200, 3), np.uint8)


print(len(data[id]))

k = cv2.KalmanFilter(6, 4, 0)
k.measurementMatrix = np.array([[1, 0, 0, 0, 0, 0],
                                [0, 1, 0, 0, 0, 0],
                                [0, 0, 1, 0, 0, 0],
                                [0, 0, 0, 1, 0, 0]], dtype=np.float32)
k.transitionMatrix = np.array([[1, 0, 1, 0, 1/2, 0],
                               [0, 1, 0, 1, 0, 1/2],
                               [0, 0, 1, 0, 1, 0],
                               [0, 0, 0, 1, 0, 1],
                               [0, 0, 0, 0, 1, 0],
                               [0, 0, 0, 0, 0, 1]], np.float32)
k.processNoiseCov = np.eye(6, dtype=np.float32)
k.measurementNoiseCov = 1e1 * np.eye(4, dtype=np.float32)
k.errorCovPost = np.eye(6, dtype=np.float32)
k.statePost = 0.1 * np.random.randn(6, 1).astype(np.float32)

pv = 0
pu = 0

for p1, p2 in zip(data[id][:-1], data[id][1:]):
    print(k.predict())
    v, u = p1
    k.correct(np.array([v, u, v - pv, u - pu], dtype=np.float32).reshape((-1, 1)))
    pv = v
    pu = u
    cv2.arrowedLine(img, tuple(p1), tuple(p2), (255, 0, 0))
    pp = np.array([v, u], dtype=np.float32).reshape((-1, 1))
    for i in range(3):
        p = k.predict()
        cv2.arrowedLine(img, tuple(pp[:2, :]), tuple(p[:2, :].reshape(-1)), (0, 255, 0))
        pp = p

v, u = data[id][-1]
pp = np.array([v, u], dtype=np.float32).reshape((-1, 1))
for i in range(30):
    p = k.predict()
    cv2.arrowedLine(img, tuple(pp[:2, :]), tuple(p[:2, :].reshape(-1)), (0, 0, 255))
    pp = p

# cv2.imshow("seq", cv2.resize(img[100:300, 600:900], fx=4, fy=4, dsize=None))
cv2.imshow("seq", img)
cv2.waitKey(0)
exit()
# moving average
ema = data[id][0]
emas = [tuple(ema)]
a = 0.6
for v, u in data[id][1:-10]:
    ema = (v * a + ema[0] * (1 - a), u * a + ema[1] * (1 - a))
    emas.append(ema)

print(emas)
# cv2.waitKey(0)
plt.plot(data[id])
# plt.hold()
plt.plot(emas)
xs = np.arange(data[id].shape[0])
ys = data[id][:, 0]
zs = data[id][:, 1]
coefs = np.polyfit(xs[15:25], ys[15:25], 2)
xs = xs.reshape((-1, 1))
print(np.hstack([xs ** 0, xs ** 1, xs ** 2]).shape)
poly = np.hstack([xs ** 0, xs ** 1, xs ** 2]).dot(coefs[::-1].reshape((-1, 1)))
plt.plot(poly)

plt.show()
