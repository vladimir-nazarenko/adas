import cv2
import numpy as np
import logging
import resolver


def main():
    logging.basicConfig(level=logging.DEBUG, filename="logs.txt")
    loader = resolver.image_loader
    objects = resolver.objects
    while loader.has_next():
        loader.next()
        display = resolver.display
        display.display(objects.get_object_set(), loader.get_left().copy())
        if cv2.waitKey(100) & 0xFF == 27:
            break


if __name__ == "__main__":
    main()
