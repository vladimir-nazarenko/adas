import configparser


def read_default_config_for_sequence(path="./default.cfg"):
    config = configparser.ConfigParser()
    config.read(path)
    return config["default"]


def read_main_config(path="./params.cfg"):
    config = configparser.ConfigParser()
    config.read(path)
    return config["PARAMS"]


def read_sequence(path, drive_id):
    config = configparser.ConfigParser()
    config.read(path)
    return config[drive_id]


def read_config(path="./params.cfg"):
    main_config = read_main_config(path)
    default_path = main_config["default_sequence_config"]
    default_sequence = read_default_config_for_sequence(default_path)
    sequence_path = main_config["sequence_config"]
    drive = main_config["drive"]
    sequence = read_sequence(sequence_path, drive)
    all_config_dict = dict(sequence)
    for k, v in dict(default_sequence).items():
        if k not in all_config_dict:
            all_config_dict[k] = v
    for k, v in dict(main_config).items():
        assert k not in all_config_dict
        all_config_dict[k] = v
    return all_config_dict


def main():
    cfg = read_config()
    for k, v in cfg.items():
        print(k, v)

if __name__ == "__main__":
    main()
