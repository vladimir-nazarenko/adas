import read_config
import KITTIRawImageLoader
import DisparityLoader
import HorizonEstimator
import StixelEstimator
import StixelObjectSetSupplier
import YoloObjectSetSupplier
import DefaultDisplay
import Calibration
import KLTObjectSetSupplier
import SkipNObjectSetSupplier
import DistanceMapLoader
import CalculateDistanceObjectSetSupplier
import ClassDisplay
import DistanceDisplay
import StereoDebugDisplay
import OcclusionHandlingObjectSetSupplier
import StixelDebugDisplay
import HorizonDebugDisplay
import KLTTrajectoryDisplay
import STPointsDisplay
import WriteVideoDisplay
import WriteImageDisplay
import FrameNumberDisplay
import ObjectIdentidierDisplay
import CollisionEstimatorObjectSetSupplier
import FilterKLTAtSidesObjectSetSupplier
import TTCDisplay
import SpeedLoader
import logging

logging.basicConfig(level=logging.DEBUG)

config = read_config.read_config()
calibration = Calibration.Calibration()
image_loader = KITTIRawImageLoader.KITTIRawImageLoader()
speed_loader = SpeedLoader.SpeedLoader(image_loader)
disparity_loader = DisparityLoader.DisparityLoader(image_loader)
distance_loader = DistanceMapLoader.DistanceMapLoader(disparity_loader)
horizon_estimator = HorizonEstimator.HorizonEstimator(disparity_loader)
stixel_estimator = StixelEstimator.StixelEstimator(horizon_estimator)

objects = FilterKLTAtSidesObjectSetSupplier.FilterKLTAtSidesObjectSetSupplier(
          CollisionEstimatorObjectSetSupplier.CollisionEstimatorObjectSetSupplier(
          OcclusionHandlingObjectSetSupplier.OcclusionHandlingObjectSetSupplier(
          CalculateDistanceObjectSetSupplier.CalculateDistanceObjectSetSupplier(distance_loader,
          StixelObjectSetSupplier.StixelObjectSetSupplier(stixel_estimator, disparity_loader,
          KLTObjectSetSupplier.KLTObjectSetSupplier(7,
          CalculateDistanceObjectSetSupplier.CalculateDistanceObjectSetSupplier(distance_loader,
          SkipNObjectSetSupplier.SkipNObjectSetSupplier(1,
          YoloObjectSetSupplier.YoloObjectSetSupplier(None, image_loader)))))))))
# )

# full debug
# display = StereoDebugDisplay.StereoDebugDisplay(
#           HorizonDebugDisplay.HorizonDebugDisplay(horizon_estimator,
#           StixelDebugDisplay.StixelDebugDisplay(stixel_estimator,
#           DistanceDisplay.DistanceDisplay(
#           ClassDisplay.ClassDisplay(
#           DefaultDisplay.DefaultDisplay(None))))))

# no debug
display = DistanceDisplay.DistanceDisplay(
          KLTTrajectoryDisplay.KLTTrajectoryDisplay(
          # ObjectIdentidierDisplay.ObjectIdentifierDisplay(
          ClassDisplay.ClassDisplay(
          DefaultDisplay.DefaultDisplay(
          FrameNumberDisplay.FrameNumberDisplay(None
          # WriteVideoDisplay.WriteVideoDisplay(True, 5, None)
          )))))
